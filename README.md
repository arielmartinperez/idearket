# Workshop Idéarket

Ce workshop a été créé avec les élèves de l'école Campus Fonderie de l'Image de Bagnolet, la semaine du 13 décembre 2013. Pendant cinq jours, ils et elles ont fait des versions personnalisées (forks) des fontes de Velvetyne et ensuite ils et elles ont fait des poster en utilisant ces fontes modifiées. L'univers graphique des fontes et des affiches a été inspiré par une hypothèse de travail de design fiction basée sur le métavers. Les histoires et personnages habitant ce monde fictionnel ont été co-créés par les élèves.

## Concept de départ (Ariel Martín Pérez)

Dans un avenir proche, des technologies intrusives dans le cerveau nous permettront de lire (et d’exporter) les pensées directement de notre esprit en tant que flux d’informations purs.

Cette technologie est exploitée par des entreprises et des gouvernements pour contrôler et manipuler les citoyen.nes, en analysant massivement leurs pensées.

Cependant, certain.es citoyen.nes se voient accorder la capacité de limiter la lecture de leurs pensées par des sources externes, ce qui leur permet de vendre leurs idées dans un lieu situé
dans le métavers.

## Description de l'idéarket (co-écrite par les élèves)

L'idéarket c'est un marché qui existe dans le métavers.

C'est une ville dont on ne peut ni sortir ni entrer qui existe dans la métavers, la mort n'existe pas, la vieillesse n'existe pas, les crimes ne sont pas punis. Pas d'églises,  pas de président

Son nom vient de la compression des mots "idée" et "market". Du coup, c'est un marché des idées dans lequel on vend et on achète toutes les dernières conceptions du monde. Des mouvements politiques, des idées artistiques, les derniers systèmes philosophiques, tout est représenté.

Il y a des vendeurs, des consommateurs, des familles, des trafiquants, des espions, des touristes, des

On peut créer son business pour faire du gros cash.

Il ressemble à vaste marché d'échanges et de rencontres, ou tout est possible.

Il ressemble au paradis dans lequel les habitants ont des cheveux blancs.

Il y a une vieille musique de fond comme dans les ascenseurs.

Il a un soleil bleu-vert qui éclaire tout avec sa lumière. Il fait toujours très chaud. On y trouve des vendeurs de citronnade partout parce que si on ne voit pas de la citronnade on se dessèche au bout d'une demie heure et son personnage aura un aspect tout rabougri.

Il a été créé en 2023. Sa création a été annoncée dès 2022 dans une campagne sur les réseaux sociaux.

Il a été créé par Yaya / PAKIRA / Zalem / Rick / Travis/Jack

il a été créé par un conglomérat

Il est découpé en différentes parties :

   1 - La place centrale : C'est l'endroit où les nouveaux joueurs se rejoignent  pour échanger sur des nouvelles informations. Il y a toujours plein d'animations, des jongleurs et des musiciens. C'est là où on peut se faire piquer les idées si on ne fait pas attention à ses poches.  Attention il y a des voleurs d'idées !

   2 -un énorme escalator infini qui monte jusqu'au ciel. On peut décider d'être visible ou invisible

   3 - Plusieurs zones/ espace, autres planètes  sont dans le ciel (Jupiter avec ses lunes, des planètes de la pop culture - voir plus loin). Il y a une gare où on peut acheter ses billets pour aller sur différentes planètes.

   4 - Un désert avec des crânes de vache et des cactus. Il n'y a pas grand monde, on peut s'y perdre facilement et ne plus jamais y retourner. Il y a des passages souterrains (cachés sous les cactus) qui permettent de retourner à la place centrale à bord de trains ultra-rapides.

   5 - une planète remplie de bisounourses et des télétubbies avec  les yeux arrachés, des cafards aussi.


   un monde où l'on perd sa forme humaine libre pour une forme dématérialisée déposséder de toutes idées
   des champs d'énergie avec des particules de lumière.
   un monde banal mais où on peut changer d'apparence indéfiniment pour échapper à son quotidien.


6 -Le sport national c'est du Mario kart  wii sport et du smash bros ultimate sans oublier CS go

7 - plusieurs Mondes ou même univers entièrement basés sur la pop culture ( ex: une planète pokémon, une planète One piece, naruto, futurama, minecraft, marvel, star wars ...........)

8 - Tout le monde s'habille en gucci Louis Vuitton Balenciaga Prada ect ect beaucoup de contrefaçon malheureusement (louis vuitton) pas de droit d'auteur, là bas la monnaie officielle est le Bitcoin

9 -Attention à la mafia qui règne dans l'ombre et qui taxe les commerçants par des pots de vins

10 tu ne peux pas mourir au pire ta juste un rhume

## Lucas Deprez - Millimètre

Mon personnage se nomme Lucy 1.0, c’est la première intelligence artificielle à avoir intégrée le Metaverse, un monde entièrement numérique. Elle a pour but de guider les voyageurs (nous, les joueurs), perdus dans ce vaste univers.

Cette I. A. à l’apparence d’une femme holographique avec un visage doux et inoffensif qui ne laisse pas apparaitre d’imperfection. Elle peut voler, traverser les murs et les objets ce qui lui permet de se déplacer où bon lui semble.

Le créateur de ce monde virtuel et de Lucy, "Zalem" se son pseudo, a relié le code source de Lucy à celle du Metaverse ce qui l’a rendu immortel à l'état numérique.  On ne connaît pas la vraie identité de “ Zalem”, ce qui rend la création du Metaverse très mystérieuse.

## Florence Babinot - Pilowlava

Personnage : Louis 37 ans, 1m90, écrivain et collectionneur
Style vestimentaire : vêtement simple mais assez chic ( manteau long, vêtements dans les tons bruns beiges, velours et cuir, chaussures cirées)
Physique : cheveux mi long jusqu’au bas du coup, un peu souvent décoiffé, lunette ronde et barbe mal taillée

Caractère : Il est assez serviable, très ouvert d’esprit, honnête, curieux, ne compte sur personne à part lui même, excentrique (son propre univers), pas comme tout le monde, déteste l’avis général, caractère fort et défend ses opinions. Il papillonne dans les endroits qui l’inspirent, il fonctionne impulsivement face à ses émotions et aime se faire guider par son instinct raisonnablement. Il garde une grande distance avec ses amis et sa famille car il ne veut pas se dévoiler et rester assez mystérieux pour pas qu’on ne profite de sa gentillesse et sa confiance. Louis est hypersensible.

Loisirs : Louis aime sortir à la bibliothèque, dans les jardins de Paris, aime aller voir son café le soir dehors dans de jolis milieux, aime rencontrer de nouvelles personnes et créer des liens avec des gens différents. Il aime aussi voyager dans les brocantes même lors de ses voyages. Il collectionne les horloges anciennes et qui ont vu le temps passer avec les gens puis s’arrêter. Il s’intéresse au temps qui passe et qui s'arrête.
Il aime apprendre de nouvelles choses, il a soif de culture devant le monde grandiose qui l’entoure. Il aime visiter des musées, et partager ce qu’il voit avec ses amis. Il aime écouter les autres avec coeur et les conseiller tout en restant très respectueux de leur choix.

Sa personne : Il aime écouter du jazz et de la musique qui le font voyager les pieds sur terre, il aime les sons mélodieux et profonds. Il fait aussi du piano, ce qui l’aide à extérioriser ses émotions parfois même les plus sombres. Il ne montre rien à l’extérieur mais sache tout chez lui en faisant ce qu’il aime quitte à lâcher quelques larmes. Il a un père absent et une mère très présente dans sa vie qu’il voit encore pour lui raconter sa vie, ses fiertés et son évolution dans la vie. Il vit seul avec ses deux chats (filo et booke), dans un appartement parisien plutôt modeste mais assez grand pour y stocker ses émotions et sa passion. Louis vit à travers les horloges et ses sentiments. Il voit quand même cette femme qui le regarde pour ce qu’il est réellement, qui est là quand il en a le plus grand besoin mais qui est là surtout pour l’encourager dans sa vie et inversement (il l’a remercie énormément pour sa présence). Il écrit beaucoup, sur la vie, le temps, les émotions et surtout ses émotions face à un monde qui coule. Il est très sensible aux problématiques actuelles : les abattoirs, le climat, la politique, l’écologie et donne de sa graine et de ses opinions pour essayer de faire bouger un peu les gens, au moins son entourage.

## Leo - Lack

"Le résistant" dans le monde réel est quelqu’un de simple. Il mesure 1,70m il a les yeux marron, son prénom est Marcus, il à une personnalité assez discrète mais dans sa tête les idées fusent il est engagé. Il travaille dans la création visuelle. Un soir en rentrant du travail il se rend dans la méta et quelque chose le trouble, il à le sentiment que ce progrès technologique est une dystopie, il veut donner du sens à sa vie en essayant de faire prendre conscience aux utilisateurs de la meta qu’ils sont dans un monde où ils n’ont aucun contrôle, que ce monde à pour but de les contrôler totalement. Qu’ils abandonnent leurs forment humaines consciente libre pour une forme dématérialisée déposséder de toutes idées.

Dans le métaverse on ne sait pas vraiment qu’il il est, c’est peut être un avatar à l’apparence humaine, peut être un avatar tout droit sorti d’un imaginaire nul ne le sait , pourtant il est là comme un nuage insaisissable. Il protège les utilisateurs et le permettant de conserver leurs idées secrètes, car dans l’idéakert en apparence on a ce sentiment de liberté pourtant on a aucun contrôle sur nos idées, les états lobbys personnes d’influence eux ont une protection sur leur idées mais ils utilisent les idées des classes moyennes pour s’enrichir. Il est seul face à une montagne d'ennemis qui le traque sans pitié, car il représente une entrave à la méta.

## Reddy Juvelys - Karrik

Mon personnage se nomme Ohmah, il est un jeune designer graphique, calligraphe et philosophe. Il est capable de voir dans l’obscurité totale. Il possède des ailes cachés dont il peut déployer en cas de danger
Il n’écoute que de la musique classique. Il porte un débardeur rouge et un pantalon noir, un collier en croix; il a des cheveux blanc et a un dragon tatoué sur son dos. Il est mystérieux, très à l'écoute, travailleur, curieux.
Il adore le cinéma. Son rêve est de devenir un artiste accompli, c'est-à-dire aider les autres autour de lui à travers ses chefs-d'œuvre. Il est très faible par la beauté. Voir quelque chose de beau ou quelqu’un  l’inspire et le rend vivant.

Visiter les expositions et les musées est son moyen de se cultiver dans le domaine de l’art.

## Gregory Ligarius - Le Murmure

Il s'appelle Rius Brabrus ,il mesure 2m70,a un œil marron et l’autre violet.
Son style de vêtement vient de la culture japonaise ?streetwear et outdoor ,et les costumes trois pièces à l'anglaise .
Rius est un bon vivant voyage beaucoup ,son caractère est trompe oeil .
Joueur de NBA Rius a de côté crée son festival de musique reconnue mondialement
Rius  a un corps athlétique, c’est le chouchou des nanas
Grâce à son festival, il crée des événements  pour les grandes marques et société .
Il a reçu le pouvoir de la lumière mais le désavantage c’est qu’il peut plus nager au risque de mourir . Mais ça personne ne le sait apprécier par un grand nombre de la population, il a aussi beaucoup de haters. Il adore voyager. La nuit il devient un bustier masqué au nom de Anbu. Rius est très futé et malin.

## Matis Corre - Cantique

Travis est un homme d'1m90 pour 78 kilos. Il fait beaucoup de sport donc il est plutôt musclé mais a des cicatrices qui recouvrent ces deux mains à cause d'un accident, on ne sait pas d'où elle vient car c'est quelqu'un d'extrêmement discret et calme il ne raconte jamais rien sur sa vie car elle est très compliqué il n'a pas grandis dans un bon environnement familiale. Il est métis originaire d'Afrique et des Antilles.

Travis a un double rôle dans la société, il est boulanger le matin et cambrioleur la nuit car il est n'est pas motivé pour trouver un travail de nuit c'est quelqu'un de paresseux. Il commence sa journée à 6h finis à 19h et pars directement ganté chez ses voisins pour voler. C'est quelqu'un de très stratège qui conçoit toujours deux ou trois plans d'avance au cas où il aurait des problèmes et s'en sort toujours.

Il ne s'est jamais fait coincer car il est très malin, son médecin lui a diagnostiqué un Q.I de 400. Il lis beaucoup de livre philosophique (volé), adore faire a manger (nourriture volée), il est même très doué dans ce domaine. Son plat préféré et qu'il adore faire et la nourriture asiatique.
Il vit seul, n'aime pas se faire des amis ou du moins s'y attacher trop longtemps mais il adore être en couple.
C'est un grand sentimentale.

## Patrick Varela - Runic

Mon personnage s’appelle Amiral et il vit sur la planète Terre. C’est un personnage qui déteste les humains, il n'aime pas vivre avec les autres. Plus tard, il aimerait créer une planète du nom Celsius (un monde virtuel) avec tous les personnages de manga qu’il aime, ou il aimerait vivre loin des humains. Il veut bâtir un monde parfait dont il sera le roi.

Amiral il aime dormir et manger et écouter du trillwave (cloud rap). Il aime tout ce qui est en rapport avec les Aliens, OVNI, etc.

## Arno Garros - Gulax

Le marché des sables est un monde qui existe dans le métavers. Comme son nom l'indique, il s'agit d’un marché qui se trouve dans le désert. Ce marché est si grand qu’il forme la plus grande ville que la planète abrite. Ce monde est un vestige de la guerre interstellaire de 2300. On retrouve dans ce marché toute sorte de business, mais la marchandise principale que l’on retrouve au marché des sables, ce sont des statues d’art en métal récupéré des vestiges des vaisseaux spéciaux que l’on retrouve un peu partout dans le désert. La plupart des être vivant dans cette ville ne sont pas humains. Les rares humains vivant dans cette ville sont des artistes qui vont chercher du métal dans le désert puis en font des statues. Ils revendent ensuite ces œuvres, à des prix misérables, aux gérants du marché. L’aire y est irrespirable à cause du vent qui fait constamment voler le sable. Ainsi les plus riches possèdent des filtreurs d’air alors que les plus pauvres se contentent d’un simple foulard très peu efficace. Les populations du marché souffrent donc de maladies pulmonaires assez fréquentes. Le marché se trouve en plein désert, les milliers de stands sont recouvert de géantes toiles en tissu pour protéger un minimum de la lumière et du soleil. Dès que l’on sort du marché et que l’on s’aventure dans le désert, une forte luminosité prospère ce qui oblige toute personne à  porter un masque de protection.
Ce monde à été créé en 2700 par la mairie de cette même ville, aujourd’hui ville riche, capitale du commerce interplanétaire. Le but de cette création est de rappeler aux joueurs d’où ils viennent et à quel point était dure la vie d’avant.
Sun est une femme d’une vingtaine d’années, d'environ 1m70, de peau blanche et cheveux noirs qui vit dans le marché des sables. Elle passe ses journées dans le désert à chercher du métal dans les vestiges de vaisseaux de guerre. Quand la nuit tombe à 15h, elle rentre pour créer des statues avec le métal récupéré la journée. Elle traîne dans le marché toute la nuit en essayant de vendre ses statues au marchand.

## Lea Buyle - Steps Mono

Marcolo, « homme de la foi ». Ce nom m'a été donné par ma mère avant qu’elle nous aient abandonné avec mon frère sur la lune. Loris n’avait que 7 ans, mais a toujours eu un esprit affuté pour comprendre les choses rapidement, et sans faire de caprices.
Cela fait maintenant quinze ans que je n’ai pas eu de retour de mon frère, et il vas fêter ses 20 ans. Mais je suis à deux doigts de le retrouver, il parait qu’il est sur un cambriolage en ce moment même.
Je n’aurais qu'à l’attendre à son endroit de départ prévu pour s’échapper. A très vite, petit frère.

Marcolo est un homme qui a deux travailles, il est détective la journée pour optique de retrouver son frère disparu, et cuisinier le soir, dans un restaurant assez particulier.
La journée, il passe son temps dans le monde actuel. Lire dans les pensées des autres, le travail de policier n'a jamais été aussi facile. Mis a part quelques dispositif vendu par le marché noir qui permet de bloqué cet acces aux pensés.
Celles-ci sont vendues et exploitées par les plus puissants sur le territoire. On a découvert que leur objectif est de condamner le plus de personnes possible pour anticiper tout éventuel surmasse de population, ou mieux encore se débarrasser de toutes personnes de classe basse à salir leur réputation? Territoire? Monde?

Mais le soir cette réalité devient aussitôt virtuelle. Ce restaurant est le parfait endroit pour trouver tout ce que tu veux trouver, lieux/ informations/ échanges etc…  Ce passage commence par s'installer dans le restaurant et puis de commander un plat spécial « Blowcase » qui vous mènera la seconde d’après dans le monde virtuel.
Par ailleurs dans ce monde les coups sont reçus comme dans la réalité, tout le reste peut être inventé, créé, vendu, acheté, divulgué et protégé. Le seul point positif de ce monde virtuel est que le créateur a renforcé ce système jusqu'à sa mort pour qu’il n'y ait personne dans ce monde qui puisse le hacker ou le détruire.
J’ai dû me renforcer physiquement comme intellectuellement pour pouvoir poursuivre mes pistes sans danger.

Marcolo a 26 ans, cheveux brun, yeux brun clair, il est physiquement fort car il a fait de la boxe et de l'aïkido.

## Sebastian Chimana - Combat

Je suis Jack, un être issu de l’évolution de l’humanité, d’environ 145 ans Je suis grand, fin et de carrure assez sportive. Je porte occasionnellement des petites lunettes à projection futuriste (c’est un petit bijou que j’ai trouvé sur la planète Izmir).

Les traits de mon visage sont assez fins, mes cheveux sont tentaculaires. J’ai des yeux jaunes.

Je pratique le rugby intergalactique pour m’amuser.
Il y a environ 130/120 ans en 2021 tout était différent.
J’avais fait des études de mode, puis avait créé ma propre marque et avait fait fortune grâce à cela.
Aujourd’hui je dirige un point de vente l’Idéarket et je vends des informations confidentielles aux milices indépendantistes afin de faire la guerre aux gouvernements et aux entreprises qui collectent les pensées de la population.
Je suis en quelque sorte un hors la loi. Mais je pense dur comme fer que ma cause est la bonne et que chacun doit avoir une liberté d'expression et le droit d’avoir une vie privée et des pensées à soi.
J’ai construit ce commerce avec Rayan mon petit frère. Nous sommes les derniers membres de ma famille. Rayan est mariée à Elise et il a trois adorables enfants.

Je suis une personne gentille, je déteste l’injustice, j’aide toujours mon prochain et je suis très loyal. J’ai malheureusement un vice qui est l’argent… Je vous en dis pas plus et vous laisse découvrir mon histoire.
